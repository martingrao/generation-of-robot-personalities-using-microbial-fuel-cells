# Generation of Robot Personalities Using Microbial Fuel Cells

In this repository is the code written by Martin Grao for his MSc Robotics dissertation (University of Bristol and the University of the West of England).

## Creation of RobotPersonality object
When creating a new *RobotPersonality*, the class allows the following optional parameters to be determined:
\begin{itemize}
* *i_w*: Initial personality weight. A number from 0 to 1 that determines the weight the model gives to the random initial personality.
* *o\_def,c\_def,e\_def,a\_def,n\_def*. NumPy arrays determining the coefficients of the definitions of each trait. Traits are defined as follows: [happiness, sadness, answers, opposite of answers, orders, opposite of orders, stability, instability]
  *Example*: o_def = np.array([1 / 3, 0, 2 / 3, 0, 0, 0, 0, 0])
* *stages*. NumPy array that determines the start times of each stage of life.
  *Example*: stages = np.array([0,100000])
* *alpha*. Number from 0 to 1 that determines the value of alpha.
* *sub*. Boolean that determines whether the subjective perception is activated.
* *amp*. Boolean that determines if the amplification of the subjective perception is activated.
* *k*. Value of the amplification constant.

## Loading data
To load data into the RobotPersonality object, use the load_data function, which has the following inputs:
* *t_mfc*: NumPy array containing the times at which the data points of the output of the artificial gut have been measured.
* *mfc*: NumPy array containing the data points of the output of the artificial gut.
* *t_o*: NumPy array containing the times at which the scores of the orders have been measured.
* *a*: NumPy array containing the scores of the orders.
* *t_a*: NumPy array containing the times at which the scores of the answers have been measured.
* *a*: NumPy array containing the scores of the answers.

## Updating the personality

To update the personality, simply call the *update_personality* function. The personality can be queried using the *get_personality* function.






