import random
import numpy as np
import copy
import matplotlib.pyplot as plt
from scipy.signal import medfilt
import pandas as pd

# Class containing the data necessary for the generation of personalities 
class PersonalityData:
    def __init__(self,):
        self.mfc_max = 0.0
        self.mfc_d_max = 0.0
        
        self.full_mfc = np.array([])
        self.full_t_mfc = np.array([])
        
        self.orders = np.array([])
        self.answers = np.array([])
        self.orders_val = None
        self.answers_val = None
        self.health = {'h++': 0, 'h+': 0, 'h-':0, 'h--': 0, "d": 0}
        self.instability = {'i++': 0, 'i+': 0, 'i-': 0, 'i--': 0}
        
        self.last_personality = None

class RobotPersonality:
    # Traits are defined as follows: [happiness, sadness, answers, opposite of answers, orders, opposite of orders, stability, instability]
    def __init__(self, i_w=0.0, stages=np.array([]), o_def=np.array([1 / 3, 0, 2 / 3, 0, 0, 0, 0, 0]),
                 c_def=np.array([0, 0, 0, 0, 0, 0, 1.0, 0]), e_def=np.array([2 / 3, 0, 1 / 3, 0, 0, 0, 0, 0]),
                 a_def=np.array([1 / 3, 0, 0, 0, 2 / 3, 0, 0, 0]), n_def=np.array([0, 2 / 3, 0, 0, 0, 1 / 3, 0, 0]),
                alpha=np.array([1.0,1.0,1.0]),sub=False,amp=False,k=30):

        self.check_all(i_w)
        self.data = PersonalityData()
        self.i_w = i_w
        self.stages = stages
        self.s = 0
        self.o_def = o_def
        self.c_def = c_def
        self.e_def = e_def
        self.a_def = a_def
        self.n_def = n_def
        self.alpha = alpha
        self.sub = sub
        self.amp = amp
        self.k = k
        
        # Create initial random personality
        self.initial_personality = self.random_personality()

        self.acquired_personality = {}
        
        #Create final personality
        self.personality = self.final_personality(self.initial_personality, self.acquired_personality, i_w)

    def get_personality(self):
        return self.personality

    def create_personality(self, o, c, e, a, n):
        personality = {"o": o, "c": c, "e": e, "a": a, "n": n}
        return personality

    # Function that returns the life stage to which a specific time corresponds
    def get_life_stage(self, t):
        s = 0
        for i in range(len(self.stages)):
            if t>self.stages[i]:
                s = i + 1
        return s
        
    def change_life_stage(self,s):
        self.s = s

    # Function to load new data to the personality
    def load_data(self, t_mfc=np.array([]), mfc=np.array([]), t_o=np.array([]), o=np.array([]), t_a=np.array([]),
                  a=np.array([])):
        # Process the output of the MFC
        if len(mfc) != 0:

            self.data.full_mfc = np.append(self.data.full_mfc, mfc)
            self.data.full_t_mfc = np.append(self.data.full_t_mfc, t_mfc)

            for i, val in np.ndenumerate(mfc):
                
                # Update the historical maximum value
                if val > self.data.mfc_max:
                    self.data.mfc_max = val
                # Update current life stage
                if self.stages.any():
                    s = self.get_life_stage(t_mfc[i])
                else:
                    s = 0

                if s > self.s:
                    self.change_life_stage(s)

                # Classify the data points of the MFC output
                if val >= 0.75 * self.data.mfc_max:
                    self.data.health["h++"] = self.data.health.get("h++") + 1
                elif val >= 0.5 * self.data.mfc_max:
                    self.data.health["h+"] = self.data.health.get("h+") + 1
                elif val >= 0.25 * self.data.mfc_max:
                    self.data.health["h-"] = self.data.health.get("h-") + 1
                elif val >= 0.0:
                    self.data.health["h--"] = self.data.health.get("h--") + 1
                else:
                    self.data.health["d"] = self.data.health.get("d") + 1

            # Calculate the difference signal of the MFC output and filter it
            ser = pd.Series(self.data.full_mfc)
            filtered_mfc = ser.rolling(10, min_periods=1).median()
            diff = np.absolute(np.diff(filtered_mfc))
            ser = pd.Series(diff)
            filtered_diff = ser.rolling(500, min_periods=1).median()
            
            # Process the difference signal
            for i, val in np.ndenumerate(filtered_diff):
                
                # Update the historical maximum difference value
                if val > self.data.mfc_d_max:
                    self.data.mfc_d_max = val

                # Classify the data points of the difference signal
                if val >= 0.75 * self.data.mfc_d_max:
                    self.data.instability["i--"] = self.data.instability.get("i--") + 1
                elif val >= 0.5 * self.data.mfc_d_max:
                    self.data.instability["i-"] = self.data.instability.get("i-") + 1
                elif val >= 0.25 * self.data.mfc_d_max:
                    self.data.instability["i+"] = self.data.instability.get("i+") + 1
                elif val >= 0.0:
                    self.data.instability["i++"] = self.data.instability.get("i++") + 1

        if len(o) != 0:
            for i, val in np.ndenumerate(o):
                # Update life stage
                if self.stages.any():
                    s = self.get_life_stage(t_o[i])
                else:
                    s = 0

                if s > self.s:
                    self.change_life_stage(s)
                self.data.orders = np.append(self.data.orders, val)
        if len(a) != 0:
            for i, val in np.ndenumerate(a):
                # Update life stage
                if self.stages.any():
                    s = self.get_life_stage(t_a[i])
                else:
                    s = 0

                if s > self.s:
                    self.change_life_stage(s)
                self.data.answers = np.append(self.data.answers, val)
                
    # Function to update the personality
    def update_personality(self):

        # Calculate happiness and stability
        if not all(value == 0 for value in self.data.health.values()):
            happiness = (2 * self.data.health["h++"] + self.data.health["h+"]) / (
                            2 * self.data.health["h++"] + self.data.health["h+"] +
                            self.data.health["h-"] + 2 * self.data.health["h--"])
            stability = (2 * self.data.instability["i++"] + self.data.instability["i+"]) / (
                            2 * self.data.instability["i++"] + self.data.instability["i+"] +
                            self.data.instability["i-"] + 2 * self.data.instability["i--"])
        else:
            happiness = 0
            stability = 1
                
        # Calculate the answer score
        if len(self.data.answers) != 0:
            self.answers_val = np.interp(np.mean(self.data.answers), (-1, 1), (0, 1))
            # Subjective perception
            if self.sub:
                sc = self.personality["a"]
                if self.amp:
                    sc = 1 / (1+np.exp(-self.k*(sc-0.5)))
                if sc <= 0.5:
                    self.answers_val = np.interp(self.answers_val,(0,1),(0,sc+0.5))
                else:
                    self.answers_val = np.interp(self.answers_val,(0,1),(sc-0.5,1))
        else:
            self.answers_val = 0.5

        # Calculate the order score
        if len(self.data.orders) != 0:
            self.orders_val = np.interp(np.mean(self.data.orders), (-1, 1), (0, 1))
            # Subjective perception
            if self.sub:
                sc = self.personality["o"]
                if self.amp:
                    sc = 1 / (1+np.exp(-self.k*(sc-0.5)))
                if sc <= 0.5:
                    self.orders_val = np.interp(self.orders_val,(0,1),(0,sc+0.5))
                else:
                    self.orders_val = np.interp(self.orders_val,(0,1),(sc-0.5,1))
        else:
            self.orders_val = 0.5

                    
        # Calculate the trait scores
        o = round(
            self.o_def[0] * happiness + self.o_def[1] * (1 - happiness) + self.o_def[2] * self.answers_val + self.o_def[
                3] * (1 - self.answers_val) + self.o_def[4] * self.orders_val + self.o_def[5] * (1 - self.orders_val) + self.o_def[
                6] * stability + self.o_def[7] * (1 - stability), 2)
        c = round(
            self.c_def[0] * happiness + self.c_def[1] * (1 - happiness) + self.c_def[2] * self.answers_val + self.c_def[
                3] * (1 - self.answers_val) + self.c_def[4] * self.orders_val + self.c_def[5] * (1 - self.orders_val) + self.c_def[
                6] * stability + self.c_def[7] * (1 - stability), 2)
        e = round(
            self.e_def[0] * happiness + self.e_def[1] * (1 - happiness) + self.e_def[2] * self.answers_val + self.e_def[
                3] * (1 - self.answers_val) + self.e_def[4] * self.orders_val + self.e_def[5] * (1 - self.orders_val) + self.e_def[
                6] * stability + self.e_def[7] * (1 - stability), 2)
        a = round(
            self.a_def[0] * happiness + self.a_def[1] * (1 - happiness) + self.a_def[2] * self.answers_val + self.a_def[
                3] * (1 - self.answers_val) + self.a_def[4] * self.orders_val + self.a_def[5] * (1 - self.orders_val) + self.a_def[
                6] * stability + self.a_def[7] * (1 - stability), 2)
        n = round(
            self.n_def[0] * happiness + self.n_def[1] * (1 - happiness) + self.n_def[2] * self.answers_val + self.n_def[
                3] * (1 - self.answers_val) + self.n_def[4] * self.orders_val + self.n_def[5] * (1 - self.orders_val) + self.n_def[
                6] * stability + self.n_def[7] * (1 - stability), 2)

        # Create acquired personality
        self.acquired_personality = self.create_personality(o, c, e, a, n)

        # Calculate final personality, and filter it with the corresponding alpha parameter
        self.personality = self.filter_personality(self.final_personality(self.initial_personality, self.acquired_personality, self.i_w),self.alpha)
        
    # Apply the exponential filter to the personality      
    def filter_personality(self, personality, alpha):
        
        if self.data.last_personality == None:
            self.data.last_personality = personality
        
        filtered_o = (1-self.alpha[self.s])*self.data.last_personality["o"] + self.alpha[self.s]*personality["o"]
        filtered_c = (1-self.alpha[self.s])*self.data.last_personality["c"] + self.alpha[self.s]*personality["c"]
        filtered_e = (1-self.alpha[self.s])*self.data.last_personality["e"] + self.alpha[self.s]*personality["e"]
        filtered_a = (1-self.alpha[self.s])*self.data.last_personality["a"] + self.alpha[self.s]*personality["a"]
        filtered_n = (1-self.alpha[self.s])*self.data.last_personality["n"] + self.alpha[self.s]*personality["n"]

        filtered_personality = self.create_personality(filtered_o,filtered_c,filtered_e,filtered_a,filtered_n)
        
        self.data.last_personality = filtered_personality
        
        return filtered_personality


    def set_personality(self, pers):
        o = pers["o"]
        c = pers["c"]
        e = pers["e"]
        a = pers["a"]
        n = pers["n"]
        self.personality = self.create_personality(o, c, e, a, n)

    def check_all(self, i_w):
        if (i_w < 0.0) or (i_w > 1.0):
            raise Exception("i_w must be a float between 0 and 1")

    def final_personality(self, in_per, acq_per, i_w):
        if (acq_per):
            o = round(in_per["o"] * i_w + (1 - i_w) * acq_per["o"], 2)
            c = round(in_per["c"] * i_w + (1 - i_w) * acq_per["c"], 2)
            e = round(in_per["e"] * i_w + (1 - i_w) * acq_per["e"], 2)
            a = round(in_per["a"] * i_w + (1 - i_w) * acq_per["a"], 2)
            n = round(in_per["n"] * i_w + (1 - i_w) * acq_per["n"], 2)
            personality = self.create_personality(o, c, e, a, n)
            return personality
        else:
            if i_w != 0:
                return in_per
            else:
                personality = self.create_personality(0.5, 0.5, 0.5, 0.5, 0.5)
                return personality

    def random_personality(self):
        o = round(random.random(), 2)
        c = round(random.random(), 2)
        e = round(random.random(), 2)
        a = round(random.random(), 2)
        n = round(random.random(), 2)
        personality = self.create_personality(o, c, e, a, n)
        return personality